package apkbuild

import (
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
)

type SecfixesTestSuite struct {
	suite.Suite
	apkbuild *os.File
}

func (suite *SecfixesTestSuite) SetupTest() {
	f, err := os.Open("testdata/APKBUILD_SECFIXES")

	suite.Require().Nil(err, "Could not open test APKBUILD")

	suite.apkbuild = f
}

func (suite *SecfixesTestSuite) TestAllElementsAreParsed() {
	secfixes, err := ParseSecfixes(suite.apkbuild)
	suite.Require().Nil(err, "Error parsing secfixes")

	suite.Require().Len(secfixes, 3)
	suite.Assert().Len(secfixes[0].Fixes, 2)
	suite.Assert().Len(secfixes[1].Fixes, 1)
	suite.Assert().Len(secfixes[2].Fixes, 1)
}

func (suite *SecfixesTestSuite) TestGetReturnsCorrectSecfix() {
	secfixes, err := ParseSecfixes(suite.apkbuild)
	suite.Require().Nil(err, "Error parsing secfixes")

	var fix *FixedVersion

	fix = secfixes.Get("1.17.2-r0")
	suite.Require().NotNil(fix, "Expected version 1.17.2-r0 to exist")
	suite.Assert().Equal("1.17.2-r0", fix.Version)

	fix = secfixes.Get("1.13.4-r0")
	suite.Require().NotNil(fix, "Expected version 1.13.4-r0 to exist")
	suite.Assert().Equal("1.13.4-r0", fix.Version)
}

func (suite *SecfixesTestSuite) TestLineNumbersAreCorrect() {
	secfixes, err := ParseSecfixes(suite.apkbuild)
	suite.Require().Nil(err, "Error parsing secfixes")

	suite.Assert().EqualValues(6, secfixes[0].LineNr)
	suite.Assert().EqualValues(7, secfixes[0].Fixes[0].LineNr)
	suite.Assert().EqualValues(8, secfixes[0].Fixes[1].LineNr)
	suite.Assert().EqualValues(9, secfixes[1].LineNr)
	suite.Assert().EqualValues(10, secfixes[1].Fixes[0].LineNr)
}

func (suite *SecfixesTestSuite) TestIdentifiersAreSplitCorrectly() {
	secfixes, err := ParseSecfixes(suite.apkbuild)
	suite.Require().Nil(err, "Error parsing secfixes")

	var fix Fix

	fix = secfixes.Get("1.17.2-r0").Fixes[0]
	suite.Require().Len(fix.Identifiers, 2)
	suite.Assert().Equal("CVE-2021-1122334", fix.Identifiers[0])
	suite.Assert().Equal("XSA-1", fix.Identifiers[1])

	fix = secfixes.Get("1.17.2-r0").Fixes[1]
	suite.Require().Len(fix.Identifiers, 3)
	suite.Assert().Equal("CVE-2021-1122335", fix.Identifiers[0])
	suite.Assert().Equal("XSA-2", fix.Identifiers[1])
	suite.Assert().Equal("XSA-3", fix.Identifiers[2])
}

func TestParseSecfixesSuite(t *testing.T) {
	suite.Run(t, new(SecfixesTestSuite))
}
